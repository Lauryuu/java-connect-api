package com.company;

import com.sun.net.httpserver.HttpServer;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.awt.*;
import java.io.IOException;
import java.net.*;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class Auth {
    private String clienId = "application-java";
    private String clientSecret = "3d0c0329-a993-4bd5-a902-a947a79092b5";
    private String redirectUri = "http://localhost:8888";
    private HttpClient client;
    private HttpServer server;

    public Auth() {
        client = HttpClient.newHttpClient();
    }

    public void openAuthorizationPage(){
        String url = "https://auth.dunarr.com/auth/realms/hoc/protocol/openid-connect/auth?client_id=%s&response_type=code&redirect_uri=%s";
        String finalUrl = String.format(url, clienId, redirectUri);
        try {
            Desktop.getDesktop().browse(new URI(finalUrl));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }
    public void runHttpServer() throws IOException {
        server = HttpServer.create(new InetSocketAddress("localhost", 8888), 0);
        server.createContext("/", new MyHandler(this));
        server.start();
    }
    public void sendAuthCode(String code) throws URISyntaxException, IOException, InterruptedException {
        server.stop(0);
        String url = "https://auth.dunarr.com/auth/realms/hoc/protocol/openid-connect/token";
        String parameters = "client_id=%s&client_secret=%s&grant_type=authorization_code&redirect_uri=%s&code=%s";
        String finalParameters = String.format(parameters, clienId, clientSecret, redirectUri, code);
        HttpRequest request = HttpRequest.newBuilder()
                .POST(HttpRequest.BodyPublishers.ofString(finalParameters))
                .uri(new URI(url))
                .header("Content-Type", "application/x-www-form-urlencoded")
                .header("Accept", "application/json")
                .build();
        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
        String content = response.body();
        JSONObject data = (JSONObject) JSONValue.parse(content);
        String accessToken = (String) data.get("access_token");
        System.out.println(accessToken);
        callApi(accessToken);
    }
    public void callApi(String token) throws URISyntaxException, IOException, InterruptedException {
        HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .uri(new URI("https://auth.dunarr.com/auth/realms/hoc/protocol/openid-connect/userinfo"))
                .header("Accept", "application/json" )
                .header("Authorization", "Bearer "+token)
                .build();
        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
        JSONObject data = (JSONObject) JSONValue.parse(response.body());
        System.out.println(data);
    }
}
