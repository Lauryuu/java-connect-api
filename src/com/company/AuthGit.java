package com.company;

import com.sun.net.httpserver.HttpServer;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.awt.*;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;

public class AuthGit {
    private String clientId = "4b3cece65a8e9446bc26dde419893be7111998e66dc182d69eb200209b29ae3e";
    private String clientSecret = "a4264063d5665effcfd600bdd917ee0e74609db4b097c74e97cca2a1fe5bd510";
    private String redirectUri = "http://localhost:8000";
    private HttpServer server;
    private HttpClient client;

    public AuthGit() {
        client = HttpClient.newHttpClient();
    }

    public void openAuthorizationPageGit(){
        String url = "https://gitlab.com/oauth/authorize?client_id=%s&redirect_uri=%s&response_type=code&state=STATE";
        String finalUrl = String.format(url, clientId, redirectUri);
        try {
            Desktop.getDesktop().browse(new URI(finalUrl));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }
    public void runHttpServerGit() throws IOException {
        server = HttpServer.create(new InetSocketAddress("localhost", 8000), 0);
        server.createContext("/", new MyHandlerGit(this));
        server.start();
    }
    public void sendAuthCode(String code) throws URISyntaxException, IOException, InterruptedException {
        server.stop(0);
        String url = "https://gitlab.com/oauth/token";
        String parameters = "client_id=%s&client_secret=%s&grant_type=authorization_code&redirect_uri=%s&code=%s";
        String finalParameters = String.format(parameters, clientId, clientSecret, redirectUri, code);
        HttpRequest request = HttpRequest.newBuilder()
                .POST(HttpRequest.BodyPublishers.ofString(finalParameters))
                .uri(new URI(url))
                .header("Content-Type", "application/x-www-form-urlencoded")
                .header("Accept", "application/json")
                .build();
        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
        String content = response.body();
        System.out.println(content);
        JSONObject data = (JSONObject) JSONValue.parse(content);
        String accessToken = (String) data.get("access_token");
        apiCall(accessToken);
    }
    public void apiCall(String token) throws URISyntaxException, IOException, InterruptedException {
        HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .uri(new URI("https://gitlab.com/api/v4/users/Lauryuu/projects?statistics=true"))
                .header("Accept", "application/json")
                .header("Authorization", "Bearer "+token)
                .build();
        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
        JSONArray data = (JSONArray) JSONValue.parse(response.body());
        for (Object project: data) {
            JSONObject projectName = (JSONObject) project;
            System.out.println(projectName.get("name") + " (" +((JSONObject) projectName.get("statistics")).get("commit_count") + " commit)");
        }
    }


//            for (Object stat: stats){
//                JSONObject infoStat = (JSONObject) stat;
//                int nbCommit = (int) infoStat.get("commit_count");




}
